# This file is part of vmod-sql
# Copyright (C) 2013-2020 Sergey Poznyakoff
#
# Vmod-sql is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Vmod-sql is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vmod-sql.  If not, see <http://www.gnu.org/licenses/>.

$Module sql 3 "SQL access functions for Varnish Cache"

COLOPHON
========
This document provides a short description of the **sql** module.
For a detailed documentation, please see the vmod-sql(3) manual page.

You will find documentation sources in subdirectory **doc** of the
**vmod-sql** source tree.

DESCRIPTION
===========
Vmod-sql  provides  functions  for accessing SQL databases from Varnish
configuration files.  It supports MySQL and PostgreSQL.

$Event sql_event
$Function INT connect(PRIV_VCL, STRING, STRING)

Description
	Initiates a connection to the database.  Returns integer value,
	which should be passed as first argument to another module's
	functions to identify the connection to operate upon.

$Function VOID connect_init(PRIV_VCL, STRING, STRING)

Description
	Same as **sql.connect**, but succeeds only if the connection is
	assigned the descriptor 0.  This function is provided as a shortcut
	to use when only one database connection is needed.

$Function BOOL query(PRIV_VCL, INT, STRING, STRING)

Description
	Performs a database query given as its second argument.  Third
	argument is a list of variable assignments separated with semicolons.
        Each assignment has the form *NAME*=*VALUE*.

	Before being executed, the query is expanded by replacing each
	occurrence of **$NAME** construct (a variable reference) with the
	corresponding *VALUE* from the third argument.  Similarly to the
	shell syntax, the variable  reference can be written as **${NAME}**. 

$Function STRING result(PRIV_VCL, INT, INT, INT)

Description
	Returns the value of the column identified by arguments 2 and 3
	(row and column numbers).  

$Function INT affected(PRIV_VCL, INT)

Description
	If the most recent query updated the database, returns the number
	of affected rows.

$Function INT ntuples(PRIV_VCL, INT)

Description
	Returns the number of tuples (rows) returned by the query.

$Function INT nfields(PRIV_VCL, INT)

Description
	Returns the number of fields in each tuple returned by the
        query.
