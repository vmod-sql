/* This file is part of vmod-sql
   Copyright (C) 2013-2018 Sergey Poznyakoff

   Vmod-sql is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   Vmod-sql is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with vmod-sql.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <config.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <regex.h>
#include <pthread.h>

struct vmod_sql_connection;

struct vmod_sql_backend {
	char *name;
	
	int (*be_init) (struct vmod_sql_connection *);
	void (*be_destroy) (struct vmod_sql_connection *);
	int (*be_connect) (struct vmod_sql_connection *);
	int (*be_disconnect) (struct vmod_sql_connection *);
	char *(*be_escape) (struct vmod_sql_connection *, const char *);
	int (*be_query) (struct vmod_sql_connection *, const char *);
	unsigned (*be_num_tuples) (struct vmod_sql_connection *);
	unsigned (*be_num_fields) (struct vmod_sql_connection *);
	int (*be_free_result) (struct vmod_sql_connection *);
	const char *(*sql_get_column)(struct vmod_sql_connection *,
					   unsigned, unsigned);
	long (*be_affected_rows)(struct vmod_sql_connection *);
};

enum {
	state_init,
	state_connected,
	state_result,
	state_error,
	state_disabled
};

#ifndef MAXCONN
# define MAXCONN 16
#endif

struct vmod_sql_connection {
	int state;                 /* Connection state */
	int debug_level;           
	struct vmod_sql_backend *backend; /* Pointer to the backend */
	char *param_str;
	char **param;
	void *data;                /* Backend-specific data */
};

void i_modsql_debug(const char *fmt, ...);
void i_modsql_error(const char *fmt, ...);

#define debug(c,n,a) do { if ((c)->debug_level>=(n)) i_modsql_debug a; } while (0)

#ifdef USE_SQL_MYSQL
struct vmod_sql_backend i_modsql_mysql_backend;
#endif
#ifdef USE_SQL_PGSQL
struct vmod_sql_backend i_modsql_pgsql_backend;
#endif

char *i_modsql_findparam(char **params, char *name);

struct vmod_sql_connection *i_modsql_conntab_get(int n);
int i_modsql_conntab_alloc(struct vmod_sql_connection *cp);

int i_modsql_create_connection(struct vmod_sql_backend *be,
			       const char *param_str, char **param);

int i_modsql_init(struct vmod_sql_connection *);
int i_modsql_connect(struct vmod_sql_connection *pd);
void i_modsql_disconnect(struct vmod_sql_connection *pd);
char *i_modsql_escape(struct vmod_sql_connection *pd, const char *input);
int i_modsql_query(struct vmod_sql_connection *pd, const char *input);
unsigned i_modsql_num_tuples(struct vmod_sql_connection *pd);
unsigned i_modsql_num_fields(struct vmod_sql_connection *pd);
void i_modsql_free_result(struct vmod_sql_connection *pd);
void i_modsql_destroy(struct vmod_sql_connection *conn);
const char *i_modsql_get_column(struct vmod_sql_connection *pd,
				unsigned row, unsigned col);
long i_modsql_affected_rows(struct vmod_sql_connection *conn);

