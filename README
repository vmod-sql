Vmod-sql README
Copyright (C) 2013-2017 Sergey Poznyakoff
See the end of file for copying conditions.

* Introduction

This file contains brief information about configuring, testing
and using vmod-sql. It is *not* intended as a replacement
for the documentation, and is provided as a brief reference only.
For accessing complete vmod-sql documentation, see the section
'Documentation' below.

* Overview

Vmod-sql is a module for Varnish Cache.  It provides API for accessing
MySQL and PostgreSQL databases from VCL scripts.

The module was tested with Varnish versions 6.0.2 -- 6.3.2.

* Installation

In order to compile the package you need to have the varnishd and
varnishapi packages installed.

Supposing that condition is met, run:

  ./configure

It should be able to automatically find the necessary components. In case 
it doesn't, tweak the configuration variables as necessary. The most 
important one is PKG_CONFIG_PATH, which contains a path (in the UNIX sense)
where the .pc files are located. It should contain a directory where the
'varnishapi.pc' file lives. Example usage:

  ./configure PKG_CONFIG_PATH=/usr/local/varnish/lib/pkgconfig:$PKG_CONFIG_PATH

Please read the file INSTALL for a detailed discussion of available variables
and command line options.

Once configured, do

  make

This will build the module.  After this step you can optionally run
'make test' to test the package.

Finally, run the following command as root:

  make install
  
* Documentation

The manual page vmod-sql(3) will be available after successful install.
To read it without actually installing the module, run
`man src/vmod-sql.3'.

An online copy of the documentation is available from
http://www.gnu.org.ua/software/vmod-sql.

* Bug reporting

Send bug reports and suggestions to <gray@gnu.org>


* Copyright information:

Copyright (C) 2013-2017 Sergey Poznyakoff

   Permission is granted to anyone to make or distribute verbatim copies
   of this document as received, in any medium, provided that the
   copyright notice and this permission notice are preserved,
   thus giving the recipient permission to redistribute in turn.

   Permission is granted to distribute modified versions
   of this document, or of portions of it,
   under the above conditions, provided also that they
   carry prominent notices stating who last changed them.


Local Variables:
mode: outline
paragraph-separate: "[ 	]*$"
version-control: never
End:
  
